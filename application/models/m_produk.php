<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_produk extends CI_Model
{
    private $_table = "produk";

    public $produk_id;
    public $name;
    public $price;
    public $image = "default.jpg";
    public $deskripsi;

    public function rules()
    {
        return [
            ['field' => 'name',
            'label' => 'Name',
            'rules' => 'required'],

            ['field' => 'price',
            'label' => 'Price',
            'rules' => 'numeric'],
            
            ['field' => 'deskripsi',
            'label' => 'Deskripsi',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["produk_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->produk_id = uniqid();
        $this->name = $post["name"];
        $this->price = $post["price"];
        $this->image = $this->_uploadImage();
        $this->deskripsi = $post["deskripsi"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->produk_id = $post["id"];
        $this->name = $post["name"];
        $this->price = $post["price"];

        if (!empty($_FILES["image"]["name"])) {
            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post['old_image'];
        }

        $this->deskripsi = $post["deskripsi"];
        return $this->db->update($this->_table, $this, array('produk_id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("produk_id" => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path'] = './upload/produk/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = $this->produk_id;
        $config['overwrite'] = true; 
        $config['max_size'] = 5000; //5mb
        
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }
    
    private function _deleteImage($id)
    {
        $produk = $this->getById($id);
        if ($produk->image != "default.jpg") {
            $filename = explode(".", $produk->image)[0];
            return array_map('unlink', glob(FCPATH."upload/produk/$filename.*"));
        }
    }
}