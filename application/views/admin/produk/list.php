<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/template/head'); ?>
    </head>
    <body class="sb-nav-fixed">        
        <?php $this->load->view('admin/template/navbar')?>
        <div id="layoutSidenav">
            <?php $this->load->view('admin/template/sidenav')?>            
            <div id="layoutSidenav_content">
                <main>
                <div class="container-fluid">
                        <h1 class="mt-4">Produk</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Produk</li>
                        </ol>                        
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                List Produk
                                <a href="<?php echo site_url('produk/add')?>"><button>Add new data</button></a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Photo</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>                                       
                                        <tbody>
                                            <?php foreach ($produk as $prod) { ?>                                                                                            
                                                <tr>
                                                    <td>
                                                    <?php echo $prod->name ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $prod->price ?>
                                                    </td>
                                                    <td>
                                                        <img src="<?php echo base_url('upload/produk/'.$prod->image) ?>"/>
                                                    </td>
                                                    <td>
                                                        <?php echo substr($prod->deskripsi, 0, 120) ?>...</td>
                                                    <td>
                                                        <a href="<?php echo site_url('produk/edit/'.$prod->produk_id) ?>"
                                                            class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                                                        <a onclick="deleteConfirm('<?php echo site_url('produk/delete/'.$prod->produk_id) ?>')"
                                                            href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <?php $this->load->view('admin/template/footer'); ?>
                <!-- Modal -->
                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/template/script');?>
        <script>
            function deleteConfirm(url) {
                $('#btn-delete').attr('href', url);
                $('#deleteModal').modal();
            }
        </script>
    </body>
</html>
