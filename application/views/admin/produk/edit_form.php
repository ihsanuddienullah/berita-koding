<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('admin/template/head'); ?>
    </head>
    <body class="sb-nav-fixed">
        <?php $this->load->view('admin/template/navbar')?>
        <div id="layoutSidenav">
            <?php $this->load->view('admin/template/sidenav')?>            
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Edit Data</h1>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Edit Data</li>
                        </ol>             
                        <?php if ($this->session->flashdata('success')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo $this->session->flashdata('success'); ?>
                            </div>
                        <?php endif; ?>                               
                        <div class="card mb-4">                            
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                Edit data
                            </div>                            
                            <div class="card-body">
                                <form action="" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $produk->produk_id?>" />
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>" type="text" name="name" placeholder="Nama Produk" value="<?php echo $produk->name ?>" />
                                        <div class="invalid-feedback">
                                            <?php echo form_error('name') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Price</label>
                                        <input class="form-control <?php echo form_error('price') ? 'is-invalid':'' ?>" type="number" name="price" min="0" placeholder="Harga Produk" value="<?php echo $produk->price ?>" />
                                        <div class="invalid-feedback">
                                            <?php echo form_error('price') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Photo</label>
                                        <input class="form-control <?php echo form_error('image') ? 'is-invalid':'' ?>" type="file" name="image" />
                                        <input type="hidden" name="old_image" value="<?php echo $produk->image ?>" />
                                        <div class="invalid-feedback">
                                            <?php echo form_error('image') ?>
                                        </div>
                                    </div>                                
                                    
                                    <div class="form-group">
                                        <label for="name">Deskripsi</label>
                                        <textarea class="form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>" name="deskripsi" placeholder="Deskripsi Produk" value="<?php echo $produk->deskripsi ?>"></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('deskripsi') ?>
                                        </div>
                                    </div>
                                    <input class="btn btn-success" type="submit" name="btn" value="save" />
                                </form>                                
                            </div>
                        </div>
                    </div>
                </main>
                <?php $this->load->view('admin/template/footer'); ?>
            </div>            
        </div>
        <?php $this->load->view('admin/template/script');?>        
    </body>
</html>
