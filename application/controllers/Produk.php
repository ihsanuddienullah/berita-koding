<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Produk extends CI_Controller 
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('M_produk');
            $this->load->library('form_validation');
        }

        public function index()
        {
            $data['produk'] = $this->M_produk->getAll();
            $this->load->view('admin/produk/list', $data);
        }

        public function add()
        {
            $produk = $this->M_produk;
            $validation = $this->form_validation;
            $validation->set_rules($produk->rules());
            
            if ($validation->run()) {
                $produk->save();
                $this->session->set_flashdata('success', 'Berhasil disimpan');
            }
    
            $this->load->view("admin/produk/new_form");
        }

        public function edit($id = null)
        {
            if (!isset($id)) {
                redirect('admin/produk');
            }

            $produk = $this->M_produk;
            $validation = $this->form_validation;
            $validation->set_rules($produk->rules());

            if ($validation->run()) {
                $produk->update();
                $this->session->set_flashdata('success', 'Berhasil Disimpan');
            }

            $data['produk'] = $produk->getById($id);
            if (!$data['produk']) {
                show_404();
            }

            $this->load->view('admin/produk/edit_form', $data);
        }

        public function delete($id = null)
        {
            if (!isset ($id)) {
                show_404;
            }

            if ($this->M_produk->delete($id)) {
                redirect(site_url('produk'));
            }
        }
    }
    